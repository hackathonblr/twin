#! /usr/bin/env node

/**
*
*
*
*
*
*
*
*
*/

// const readLine = require("../lib/input.js");
const installPackages = require("../lib/installPackages.js");
const {
    generateDefinition
} = require("../lib/createDefinition.js");
const fileIO = require("../lib/fileIO.js");
const log4js = require("log4js");
const logger = log4js.getLogger("CodeGen");
const inquirer = require("inquirer");
const prompt = inquirer.createPromptModule();
const questions = require("../lib/questions").questions;
const {
    createAppjsfile
} = require("../projectSkeletons/app.js");
const {
    createController
} = require("../projectSkeletons/controller.js");
const {
    centralController
} = require("../projectSkeletons/centralController.js");
const {
    generateYaml
} = require("../projectSkeletons/generateYaml.js");
const {
    generateFolderStructure
} = require("../projectSkeletons/createProjectStructure");

global.logger = logger;
var config = {
    "framework": "express",
    "path": "../",
    "singleController": true,
    "newProject": true,
    "db": "db",
    "puttuName": false,
    "npmInstall": false
};
var data = {};

function init(userConfigData) {
    console.log("fromCodegen ",userConfigData);
    config.basePath = userConfigData.basePath ? userConfigData.basePath : "/";
    config.port = userConfigData.port ? userConfigData.port : 10010;
    data.definition = userConfigData.jsonSchema;
    config.projectName = data.name = data.collectionName = data.modelName = data.api = userConfigData.collectionName;
    console.log(config.basePath);
    // prompt(questions)
    // .then(result => {
    // console.log(JSON.stringify(data,null,4));
    // console.log(JSON.stringify(config,null,4));
    return startProcessing()
    .then(() => {
        logger.info("Your project structure is ready");
        // readLine.closeIO();
    }).catch(err => {
        return Promise.reject();
        logger.error(" err is",err);
        // readLine.closeIO();
    });
}

function startProcessing() {
    console.log("config in startProcessing ", config);
    var promise = config.newProject ? generateFolderStructure(config) :
    Promise.resolve();
    return promise
    .then(() => config.npmInstall ? installPackages.install(config.projectName, config.path) : Promise.resolve())
    // .then(() => fileIO.readFile(config.configFile))
    .then(() => {
        config.configFileData = data;
        return generateDefinition(config.path + config.projectName, data);
    })
    .then(() => config.newProject ? createAppjsfile(config) : Promise.resolve())
    .then(() => createController(config))
    .then(() => centralController(config))
    .then(() => generateYaml(config));
}

var ob = [{
    "basePath": "api/naya2",
    "port": 10010,
    "jsonSchema": {
        "_id": {
            "type": "String"
        },
        "ifsc_code": {
            "type": "String",
            "enum": ["value1", "value2"]
        },
        "bank_name": {
            "type": "String",
            "unique": true
        },
        "branch_address": {
            "type": "String",
            "required": true
        },
        "complexObject": {
            "definition": {
                "field1": {
                    "type": "String"
                },
                "field2": {
                    "type": "Number",
                    "enum": [1, 2, 3, 4]
                }
            }
        },
        "complexObjectArray": [{
            "definition": {
                "field1": {
                    "type": "String",
                    "default": "Abcd"
                },
                "field2": {
                    "type": "Number"
                }
            }
        }],
        "simpleArray": [{
            "type": "String"
        }]
    },
    "collectionName": "try1"
},
{
    "basePath": "api/naya1",
    "port": 3001,
    "jsonSchema": {
        "_id": {
            "type": "String"
        },
        "ifsc_code": {
            "type": "String",
            "enum": ["value1", "value2"]
        },
        "bank_name": {
            "type": "String",
            "unique": true
        },
        "branch_address": {
            "type": "String",
            "required": true
        },
        "complexObject": {
            "definition": {
                "field1": {
                    "type": "String"
                },
                "field2": {
                    "type": "Number",
                    "enum": [1, 2, 3, 4]
                }
            }
        },
        "complexObjectArray": [{
            "definition": {
                "field1": {
                    "type": "String",
                    "default": "Abcd"
                },
                "field2": {
                    "type": "Number"
                }
            }
        }],
        "simpleArray": [{
            "type": "String"
        }]
    },
    "collectionName": "try2"
}
]


function processArray(i){
    if(i==ob.length-1){
        return init(ob[i]);
    }else{
        init(ob[i]).then(()=>{processArray(i+1)});
    }
}

    module.exports = {
        init
    };
