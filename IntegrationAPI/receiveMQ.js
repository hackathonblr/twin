var ObjectId = require('mongodb').ObjectID;
var mongo = require('mongodb');
var amqp = require('amqplib/callback_api');
var url = 'mongodb://localhost/db';
var Promise = require('bluebird');
var mongoClient = Promise.promisifyAll(require('mongodb')).MongoClient;

function cudMongo(msg) {
    return mongoClient.connectAsync(url)
        .then(function (db) {
            console.log('Connection established to', url);
            var messageInJson = JSON.parse(msg.content);
            var col = db.collection(messageInJson.schemaName);
            if (messageInJson.operation == "insert") {
                return col.insert(messageInJson.data);
            } else if (messageInJson.operation == "update") {
                return col.update({
                    "_id": new ObjectId(messageInJson._id)
                }, {
                    $set: messageInJson.data
                });
            } else if (messageInJson.operation == "delete") {
                return col.remove(messageInJson.data, true);
            } else {
                throw new Error('operation ' + messageInJson.operation + ' does not exist');
            }
        })
        .catch(function (err) {
            throw err;
        });
}

function recordInMongo(resObj) {
    var twinDBUrl = 'mongodb://localhost/Twin';
    mongoClient.connect(twinDBUrl, function (err, db) {
        if (err) throw err;
        db.collection("CoreService").insertOne(resObj, function (err, res) {
            if (err) throw err;
            console.log("Inserted " + resObj);
            db.close();
        });
    });
}



function init() {
    amqp.connect('amqp://localhost', function (err, conn) {
        conn.createChannel(function (err, ch) {
            var q = 'twinQueue';

            ch.assertQueue(q, {
                durable: false
            });
            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
            ch.consume(q, function (msg) {
                cudMongo(msg).then((result) => {
                    ch.ack(msg);
                    var resp = {};
                    resp.message = JSON.parse(msg.content);
                    resp.result = result;
                    console.log("response after promise from mongo", resp);
                    recordInMongo(resp);
                });
            }, {
                noAck: false
            });
        });
    });
}

module.exports = {
    init
};