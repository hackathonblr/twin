'use strict';
const codegen = require('../../../swagger-mongoose-codegen-Hackathon/bin/codeGenerator.js')
var mongo = require('mongodb');
var MongoClient = process.env.MONGO_URL || mongo.MongoClient;
var url = 'mongodb://localhost/Twin';
const definition = require("../helpers/definition.js").definition
var e = {};
var async = require('async');
var exec = require("child_process").exec;
var amqp = require('amqplib/callback_api');
var collection;
var data={};
e.buildSchema = (req, res) => {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            res.status(400);
            var response = JSON.stringify({
                result: "failed to conenct to mongo",
            });
            res.send(response);
            return;
        } else {
            var data = req.body;
            collection = db.collection('SchemaCollection');
            processArray(0).then((resObj) => {
                res.status(resObj.status);
                res.send(JSON.stringify(resObj.body));
            });
        }

    });
}

var resultArray=[];

function processArray(i){
    if(i==ob.length-1){
        return new Promise((resolve,reject)=>{
            init(data[i]).then(()=>{
              var temp = {
                schema: data[i],
                status: "success"
              };
              resultArray.push(temp);
              resolve();
            },()=>{
              var temp = {
                schema: data[i],
                status: "failed"
              };
              resultArray.push(temp);
              var resObj = {};
              resObj.body = resultArray;
              resObj.status = 200;
              resolve(resObj);
            });
        })
    }else{
        init(data[i]).then(()=>{
            var temp = {
              schema: data[i],
              status: "success"
            };
            resultArray.push(temp);
            processArray(i+1)
          },()=>{
            var temp = {
              schema: data[i],
              status: "failed"
            };
            resultArray.push(temp);
            processArray(i+1)
          });
    }
}

e.deploySchema = (req, res) => {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
            res.status(400);
            var response = JSON.stringify({
                result: "failed to conenct to mongo",
            });
            res.send(response);
            return;
        } else {
            //HURRAY!! We are connected. :)
            console.log('Connection established to', url);
            var collection = db.collection('SchemaCollection');
            collection.find({}).toArray(function (err, resultSchema) {
                if (err) {
                    console.log(err);
                    var response = JSON.stringify({
                        result: "failed to get schema",
                    });
                    res.status(400);
                    res.send(response);
                }

                if (resultSchema.length) {
                    console.log("resultschema", resultSchema)

                    for (var i = 0; i < resultSchema.length; i++) {
                        var folder = "\.\.\/" + resultSchema[i].schema.collectionName + "\/app.js";
                        console.log("path ", folder);
                        exec('node ' + folder, function (err, out, code) {
                            if (err instanceof Error)
                                throw err;
                            process.stderr.write(err);
                            process.stdout.write(out);
                            process.exit(code);
                            console.log("error");
                        });
                        console.log("Got schema details");
                    }

                }
                var response = JSON.stringify({
                    result: "Started Applications related to schema",
                });
                res.status(200);
                res.send(response);

            })
        }
    });

}
e.pushMQ = (req, res) => {
    console.log("req", req.body);
    var body = req.body;
    //Put to MQ
    amqp.connect('amqp://localhost', function (err, conn) {
        if (err) {
            console.log('Unable to connect to the rabbit server. Error:', err);
            var response = JSON.stringify({
                result: "failed to connect to rabbit",
            });
            res.status(400);
            res.send(response);
            return;
        }
        conn.createChannel(function (err, ch) {
            var q = 'ConnectorQueue';

            ch.assertQueue(q, {
                durable: false
            });
            // Note: on Node 6 Buffer.from(msg) should be used
            ch.sendToQueue(q, new Buffer(JSON.stringify(body)));
            console.log(" [x] Sent 'Message'");
        });
        var response = JSON.stringify({
            result: "Sent Message",
        });
        res.status(200);
        res.send(response);
        setTimeout(function () {
            conn.close();
        }, 500);
    });


}

function insertToMongo(item) {
    collection.insertOne(item, function (err, resultmessage) {
        if (err) {
            logger.error("failed to Insert due to system error")
        } else {
            logger.info("Successfully inserted");
        }
    });
}

function getPromiseResolveFn(data) {
    return new Promise(function (resolve, reject) {
        var sArray = [];
        async.forEachOf(data, function (result, i, callback) {
            definition.jsonSchema = data[i].jsonSchema;
            codegen.init(data[i]).then(() => {
                //if success
                var temp = {
                    schema: data[i],
                    status: "success"
                };
                sArray.push(temp);
                callback();
            }, function error() {
                //if error
                var temp = {
                    schema: data[i],
                    status: "failed"
                };
                sArray.push(temp);
                callback();
            })

        }, function () {
            sArray.forEach((item) => {
                item.status === "success" ? insertToMongo(item) : console.log("Failed Item");
            });
            //done with above function
            var resObj = {};
            resObj.body = sArray;
            resObj.status = 200;
            resolve(resObj);
        })
    })

}

module.exports = e;
