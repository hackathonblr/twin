var definition = [{
    "_id": {
        "type": "String",
        "required": true
    },
    "collectionName": {
        "type": "String",
        "unique": true,
        "required": true,

    },
    "portNo": {
        "type": "Number",
        "required": true

    },

    "jsonSchema": {
        "type": "Object"
    },
    "basePath": {
        "type": "String",
    }
}];
module.exports.definition = definition;